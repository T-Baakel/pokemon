# Pokemon | Pokedex


This project is an [Angular] project version 12.2.8.

Powered with the API `https://pokeapi.co`.

And the UI framework `https://getbootstrap.com`

## Description

This is a small Pokedex to visualize all the existing Pokemon, and their respectives statistics.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
