import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { pokemon, types } from 'src/app/interface/pokemon';
import { PokeListService } from 'src/app/service/poke-list.service';
import { TypesService } from 'src/app/service/types.service';

@Component({
  selector: 'app-poke-list',
  templateUrl: './poke-list.component.html',
  styleUrls: ['./poke-list.component.scss']
})
export class PokeListComponent implements OnInit {

  loading: boolean = false;
  loadingDetails: boolean = false;

  all: boolean = false;

  search: FormControl = new FormControl('')

  list: pokemon[] = [];
  errorList: boolean = true;
  details: any;
  errorDetails: boolean = true;
  detailsType: types[] = [];


  
  constructor(private pokeListService: PokeListService, private pokeType: TypesService) { }
  
  ngOnInit(): void {
    this.loading = true;
    this.pokeListService.pokeList().subscribe(
      (data) => {
        this.loading = false;
        this.list = data.results;
      },
      (err) => {
        this.loading = false;
        this.errorList = true;
      }
    )
  }

  getMore(): void {
    this.loading = true;
    this.pokeListService.nextPokeList().subscribe(
      (data) => {
        this.loading = false;
        if (data.next === null) {
          this.all = true;
        }
        this.list = this.list.concat(data.results);
      },
      (err) => {
        this.loading = false;
        this.errorList = true;
      }
    )
  }

  getDetails(name: string): void {
    this.loadingDetails = true;
    this.details = null;
    this.pokeListService.pokeDetails(name.toLowerCase()).subscribe(
      (data) => {
        this.loadingDetails = false;
        this.details = data;
        this.detailsType = this.pokeType.getListOfTypes(data.types);
      },
      (err) => {
        this.loadingDetails = false;
        this.errorDetails = true;
      }
    )
  }

  formatNumber(nb: number): string {
    return ("000" + nb).slice(-4)
  }

 formatTitle(title: string) {
    if (!title) return title;
    return title[0].toUpperCase() + title.substr(1).toLowerCase();
  }
}
