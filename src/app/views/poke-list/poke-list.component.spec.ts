import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { PokeListService } from 'src/app/service/poke-list.service';
import { TypesService } from 'src/app/service/types.service';

import { PokeListComponent } from './poke-list.component';

describe('PokeListComponent', () => {
  let component: PokeListComponent;
  let fixture: ComponentFixture<PokeListComponent>;

  let PokeListServiceStub: Partial<PokeListService> = {
    pokeList() {
      return of(
        {
          count: 0,
          next: "",
          previous: "",
          results: []
        }
      );
    }
  }

  let TypesServiceStub: Partial<TypesService> = {
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PokeListComponent],
      providers: [
        { provide: PokeListService, useValue: PokeListServiceStub },
        { provide: TypesService, useValue: TypesServiceStub }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should format number', () => {
    expect(component.formatNumber(4)).toEqual("0004");
    expect(component.formatNumber(123)).toEqual("0123");
  });

  it('should format title', () => {
    expect(component.formatTitle("title")).toEqual("Title");
  });
});
