import { TestBed } from '@angular/core/testing';

import { TypesService } from './types.service';

describe('TypesService', () => {
  let service: TypesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TypesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should give the right types colors', () => {
    let data = [
      {slot: 1, type: {name: "water", url: "url"}},
      {slot: 2, type: {name: "fire", url: "url"}}
    ];

    let result = service.getListOfTypes(data);

    expect(result.length === 2).toBeTruthy();
    expect(result.find(x => x.name === "fire")?.color).toBe("#F08030");
    expect(result.find(x => x.name === "water")?.color).toBe("#6790F0");
  });
  
});
