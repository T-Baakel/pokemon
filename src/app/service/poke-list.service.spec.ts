import { TestBed } from '@angular/core/testing';

import { PokeListService } from './poke-list.service';

import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('PokeListService', () => {
  let service: PokeListService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(PokeListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
