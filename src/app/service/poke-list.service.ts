import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { pokeList } from '../interface/pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokeListService {

  offset: number = 0
  constructor(private http: HttpClient) { }

  pokeList(): Observable<pokeList> {
    return this.http.get<pokeList>(environment.endpoint + `/pokemon?limit=100&offset=${this.offset}`);
  }

  nextPokeList(): Observable<pokeList> {
    this.offset += 100;
    return this.http.get<pokeList>(environment.endpoint + `/pokemon?limit=100&offset=${this.offset}`);
  }

  pokeDetails(name: string) {
    return this.http.get<any>(environment.endpoint + `/pokemon/${name}`);
  }
}
