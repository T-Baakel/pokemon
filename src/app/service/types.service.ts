import { Injectable } from '@angular/core';
import { types } from '../interface/pokemon';


@Injectable({
  providedIn: 'root'
})
export class TypesService {

  types: types[] = [
    { name: "normal", color: "#A8A878" },
    { name: "fire", color: "#F08030" },
    { name: "fighting", color: "#C03028" },
    { name: "poison", color: "#9F40A0" },
    { name: "flying", color: "#A890F0" },
    { name: "ground", color: "#A8B720" },
    { name: "rock", color: "#B8A039" },
    { name: "bug", color: "#A8B720" },
    { name: "ghost", color: "#705898" },
    { name: "steel", color: "#B8B8D0" },
    { name: "water", color: "#6790F0" },
    { name: "grass", color: "#78C84F" },
    { name: "electric", color: "#F9CF30" },
    { name: "psychic", color: "#F85888" },
    { name: "ice", color: "#98D8D8" },
    { name: "dragon", color: "#7038F8" },
    { name: "fairy", color: "#EE99AC" },
    { name: "dark", color: "#705848" },
    { name: "unknown", color: "#67A090" },
    { name: "shadow", color: "#191919" }
  ]

  constructor() { }

  getListOfTypes(type: {slot: number, type: {name: string, url: string}}[]) {
    let result: types[] = [];
    type.forEach(x => {
      const find = this.types.find(type => type.name === x.type.name);
      if (find)
        result.push(find);
    })
    return result;
  }
}
