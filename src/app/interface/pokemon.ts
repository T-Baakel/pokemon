export interface pokemon {
  name: string,
  url: string
}

export interface pokeList {
    count: number,
    next: string,
    previous: string,
    results: Array<pokemon>,
}

export interface types {
  name: string,
  color: string,
}


// export interface pokemonDetails 