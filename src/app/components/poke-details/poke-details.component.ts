import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-poke-details',
  templateUrl: './poke-details.component.html',
  styleUrls: ['./poke-details.component.scss']
})
export class PokeDetailsComponent implements OnInit {

  @Input() details: any;

  constructor() { }

  ngOnInit(): void {
  }

}
